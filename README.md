## This is a repository for my portfolio website.##

###Instructions

- Browse to **php** folder.
- Download and put [Nette PHP framework](https://nette.org/en/download) inside `php` folder.
- Create a file `constants.php` with the following:
 

```
#!php

 <?php
 
 defined('MAILING_HOST') ? null : define("MAILING_HOST", <MAILING_HOST>);
 defined('MAILING_USERNAME')   ? null : define("MAILING_USERNAME", <MAILING_USERNAME>);
 defined('MAILING_PASSWORD')   ? null : define("MAILING_PASSWORD", <MAILING_PASSWORD>);
 defined('MAILING_PORT')   ? null : define("MAILING_PORT", <MAILING_PORT>);
 
 ?>
```
 
 - This file is required in the `contact.php` mail handler which sents an email when customers contact you.