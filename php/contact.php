<?php


/* DO NOT EDIT BELOW */

/* use classes */


use Nette\Mail\Message,
	Nette\Mail\SmtpMailer;

require_once 'nette.phar';
require_once 'constants.php';

$configurator = new Nette\Configurator;
$configurator->setTempDirectory(__DIR__ . '/temp');
$container = $configurator->createContainer();

/* get post */

$httpRequest = $container->getService('httpRequest');
$httpResponse = $container->getService('httpResponse');

$post = $httpRequest->getPost();

if ($httpRequest->isAjax()) {
	$from = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
	$sendTo = 'rsnm95@gmail.com';
	$message = filter_var($_POST['message'], FILTER_SANITIZE_STRING);
	$name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
	$surname = filter_var($_POST['surname'], FILTER_SANITIZE_STRING);
	$phone =  filter_var($_POST['phone'], FILTER_SANITIZE_STRING);
	$website =  filter_var($_POST['website'], FILTER_SANITIZE_URL);
	$budget =  filter_var($_POST['budget'], FILTER_SANITIZE_STRING);
	$subject = 'Message from roosnam.com';

	$fields = array('name' => $name, 'surname' => $surname, 'phone' => $phone, 'email' => $from, 'message' => $message
	, 'website' => $website, 'budget' => $budget);
	$htmlHeader = '';
	$htmlFooter = '';
	$okMessage = 'Contact form succesfully submitted. Thank you, I will get back to you soon!';

	$htmlContent = '<h1>New message from contact form</h1>';

	$htmlContent .= '<table>';
	foreach ($fields as $key => $value) {
		if (isset($fields[$key])) {
			$htmlContent .= "<tr><th>$key</th><td>$value</td></tr>";
		}
	}
	$htmlContent .= '</table>';

	/* compose html body */

	$htmlBody = $htmlHeader . $htmlContent . $htmlFooter;

	$mail = new Message;
	$mail->setFrom($from)
		->addTo($sendTo)
		->setSubject($subject)
	    ->setHtmlBody($htmlBody, FALSE);


    $mailer = new SmtpMailer(array(
        'host'      => MAILING_HOST,
        'username'  => MAILING_USERNAME,
        'password'  => MAILING_PASSWORD,
        'secure'   => 'ssl',
        'port'	   => '465'
    ));

	try {
		$mailer->send($mail);
	} catch (Exception $e) {
		error_log("ERROR: ".$e);
	}

    $responseArray = array('type' => 'success', 'message' => $okMessage);

    $httpResponse->setCode(200);
    $response = new \Nette\Application\Responses\JsonResponse($responseArray);
    $response->send($httpRequest, $httpResponse);
}




